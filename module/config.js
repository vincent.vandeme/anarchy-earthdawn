
export const EARTHDAWN = {
  settings: {
    skillSet: 'EARTHDAWN.settings.skillSet',
    damageMode: 'EARTHDAWN.settings.damageMode',
  },
  skill: {
    athletisme: 'EARTHDAWN.skill.athletisme',
    acrobaties: 'EARTHDAWN.skill.acrobaties',
    armesLourdes: 'EARTHDAWN.skill.armesLourdes',
    corpsACorps: 'EARTHDAWN.skill.corpsACorps',
    furtivite: 'EARTHDAWN.skill.furtivite',
    pilotageAerien: 'EARTHDAWN.skill.pilotageAerien',
    pilotageFluvial: 'EARTHDAWN.skill.pilotageFluvial',
    tir: 'EARTHDAWN.skill.tir',
    combatAstral: 'EARTHDAWN.skill.combatAstral',
    invocation: 'EARTHDAWN.skill.invocation',
    sorcellerie: 'EARTHDAWN.skill.sorcellerie',
    survie: 'EARTHDAWN.skill.survie',
    artisanat: 'EARTHDAWN.skill.artisanat',
    ingenierie: 'EARTHDAWN.skill.ingenierie',
    pistage: 'EARTHDAWN.skill.pistage',
    soins: 'EARTHDAWN.skill.soins',
    art: 'EARTHDAWN.skill.art',
    comedie: 'EARTHDAWN.skill.comedie',
    dressage: 'EARTHDAWN.skill.dressage',
    etiquette: 'EARTHDAWN.skill.etiquette',
    intimidation: 'EARTHDAWN.skill.intimidation',
    negotiation: 'EARTHDAWN.skill.negotiation',
    persuasion: 'EARTHDAWN.skill.persuasion',
  },

};
