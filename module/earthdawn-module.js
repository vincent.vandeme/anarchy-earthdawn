import { MODULE_NAME, STYLE_PATH } from './constants.js';
import { EARTHDAWN_SKILLS } from './skills.js';

const EARTHDAWN_CHECKBARS_HACK = {
  plot: {
    iconChecked: ANARCHY_ICONS.iconPath(`${STYLE_PATH}/danger-point.webp`, 'checkbar-img'),
    iconUnchecked: ANARCHY_ICONS.iconPath(`${STYLE_PATH}/danger-point-off.webp`, 'checkbar-img')
  },
  anarchy: {
    iconChecked: ANARCHY_ICONS.iconPath(`${STYLE_PATH}/anarchy-point.webp`, 'checkbar-img'),
    iconUnchecked: ANARCHY_ICONS.iconPath(`${STYLE_PATH}/anarchy-point-off.webp`, 'checkbar-img')
  },
  sceneAnarchy: {
    iconChecked: ANARCHY_ICONS.iconPath(`${STYLE_PATH}/anarchy-point-scene.webp`, 'checkbar-img'),
    iconUnchecked: ANARCHY_ICONS.iconPath(`${STYLE_PATH}/anarchy-point-off.webp`, 'checkbar-img')
  }
}

const EARTHDAWN_ANARCHY_HACK = {
  id: MODULE_NAME,
  name: 'Earthdawn Anarchy hack',
  hack: {
    checkbars: () => EARTHDAWN_CHECKBARS_HACK
  }
}

export class EarthdawnModule {
  static start() {
    const earthdawnModule = new EarthdawnModule();
    Hooks.once('init', async () => await earthdawnModule.onInit());
  }

  async onInit() {
    game.modules.earthdawnModule = this;
    Hooks.on(ANARCHY_HOOKS.ANARCHY_HACK, register => register(EARTHDAWN_ANARCHY_HACK));
    Hooks.on(ANARCHY_HOOKS.PROVIDE_SKILL_SET, provide => {
      provide('earthdawn-anarchy', 'Earthdawn Anarchy Hack - FR', EARTHDAWN_SKILLS.map(it => {
        it.labelkey = `EARTHDAWN.skill.${it.code}`;
        return it;
      }));
    });
  }
}
