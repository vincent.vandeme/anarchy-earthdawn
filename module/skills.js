const { ANARCHY_SYSTEM, ICONS_SKILLS_PATH, SYSTEM_NAME, TEMPLATE } = ANARCHY_CONSTANTS;

const ATTR = TEMPLATE.attributes;
const DEFENSE = ANARCHY_SYSTEM.defenses;

export const EARTHDAWN_SKILLS = [
  { code: 'athletisme', attribute: ATTR.strength, icon: `${ICONS_SKILLS_PATH}/athletics.svg` },
  { code: 'acrobaties', attribute: ATTR.agility, icon: `${ICONS_SKILLS_PATH}/escape-artist.svg` },
  { code: 'armesLourdes', attribute: ATTR.agility, icon: `${ICONS_SKILLS_PATH}/vehicle-weapons.svg`, defense: DEFENSE.physicalDefense },
  { code: 'corpsACorps', attribute: ATTR.agility, icon: `${ICONS_SKILLS_PATH}/close-combat.svg`, defense: DEFENSE.physicalDefense },
  { code: 'furtivite', attribute: ATTR.agility, icon: `${ICONS_SKILLS_PATH}/stealth.svg` },
  { code: 'pilotageAerien', attribute: ATTR.agility, /* icon: `${ICONS_SKILLS_PATH}/piloting-ground.svg` */ },
  { code: 'pilotageFluvial', attribute: ATTR.agility, /* icon: `${ICONS_SKILLS_PATH}/piloting-other.svg`*/ },
  { code: 'tir', attribute: ATTR.agility, icon: `${ICONS_SKILLS_PATH}/projectile-weapons.svg`, defense: DEFENSE.physicalDefense },

  { code: 'combatAstral', attribute: ATTR.willpower, icon: `${ICONS_SKILLS_PATH}/astral-combat.svg`, defense: DEFENSE.astralDefense },
  { code: 'invocation', attribute: ATTR.willpower, hasDrain: true, icon: `${ICONS_SKILLS_PATH}/conjuring.svg` },
  { code: 'sorcellerie', attribute: ATTR.willpower, hasDrain: true, icon: `${ICONS_SKILLS_PATH}/sorcery.svg` },
  { code: 'survie', attribute: ATTR.willpower, icon: `${ICONS_SKILLS_PATH}/survival.svg` },

  { code: 'artisanat', attribute: ATTR.logic,/* icon: `${ICONS_SKILLS_PATH}/biotech.svg` */ },
  { code: 'ingenierie', attribute: ATTR.logic, icon: `${ICONS_SKILLS_PATH}/engineering.svg` },
  { code: 'pistage', attribute: ATTR.logic, icon: `${ICONS_SKILLS_PATH}/tracking.svg` },
  { code: 'soins', attribute: ATTR.logic, icon: `${ICONS_SKILLS_PATH}/biotech.svg` },

  { code: 'art', attribute: ATTR.charisma, isSocial: true, /* icon: `${ICONS_SKILLS_PATH}/con-art.svg`*/ },
  { code: 'comedie', attribute: ATTR.charisma, isSocial: true, icon: `${ICONS_SKILLS_PATH}/con-art.svg` },
  { code: 'dressage', attribute: ATTR.charisma, icon: `${ICONS_SKILLS_PATH}/animals.svg` },
  { code: 'etiquette', attribute: ATTR.charisma, isSocial: true, icon: `${ICONS_SKILLS_PATH}/etiquette.svg` },
  { code: 'intimidation', attribute: ATTR.charisma, isSocial: true, icon: `${ICONS_SKILLS_PATH}/intimidation.svg` },
  { code: 'negotiation', attribute: ATTR.charisma, isSocial: true, icon: `${ICONS_SKILLS_PATH}/negotiation.svg` },
  { code: 'persuasion', attribute: ATTR.charisma, isSocial: true, /*icon: `${ICONS_SKILLS_PATH}/negotiation.svg`*/ },
]
